# Imprimantes 3D

![Prusa MK3](img/PrusaSlicer/PRUSA_1.jpeg)

## Spécifications (Prusa i3 MK3/S/S+)

*	Surface d’impression (XY) : **21cm x 21cm**
*	Hauteur d’impression maximale (Z): **25cm**
*	Diamètre de buse (au FabLab ULB) : **0.6mm**
*   Logiciel (slicer) : [PrusaSlicer](https://www.prusa3d.com/fr/page/prusaslicer_424/)

![Prusa Slicer](img/PrusaSlicer/1.png){width=75%}

Les formats de fichier reconnus par le slicer sont **.stl**, **.obj** et **3mf**. 

À la première ouverture du slicer, nous vous conseillons de le mettre en mode **Avancé** ou **Expert** afin de pouvoir accéder à tous les réglages d'impression décrits dans ce tutoriel.

![Mode Expert](img/PrusaSlicer/1_1_Expert.png){width=75%}

## Préparation du G-code

### Importation du fichier

Dans le slicer, vous pouvez importer un ou plusieurs fichier en cliquant sur le bouton **Ajouter** ou en glissant les fichiers dans le slicer.

![Bouton "Ajouter"](img/PrusaSlicer/1_2_Importation.png){width=75%}

### Orientation du modèle

Si possible, orientez le modèle pour que la plus grande surface plane soit en contact avec le plateau.

Le moyen le plus simple est de cliquer sur le bouton **Positionner sur la surface** (image ci-dessous) puis de sélectionner la surface sur le modèle.

![Bouton "Positionner sur la surface"](img/PrusaSlicer/1_3_Positionner.png){width=75%}
![Sélection de surface](img/PrusaSlicer/1_4_Positionner.png){width=75%}
![Modèle orienté sur une surface plane](img/PrusaSlicer/1_5_Positionner.png){width=75%}

Ensuite, pour positionner automatiquement et de façon optimale les modèles sur le plateau, cliquez sur le bouton **Arranger**.

![Bouton "Arranger"](img/arrange.png){width=75%}

### Sélection de l'imprimante

Vérifiez le modèle de votre imprimante. Il est écrit sur une étiquette à l'arrière :

![Étiquette](img/label.png){width=75%}

Dans la liste **Imprimante**, sélectionnez le modèle de l'imprimante.

![Liste "Imprimante"](img/printer.png){width=75%}

Si le modèle en question n'est pas dans la liste :

* Cliquez sur **Ajouter/Supprimer des imprimantes**
* Cherchez le modèle correspondant
* Cochez la case **0.6mm buse**
* Cliquez sur le bouton **Fin**

Vous pouvez suivre le __[guide suivant](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/Setting-Change.md?ref_type=heads)__ 
pour le changement du modèle.

### Sélection du filament

**Attention !** Les filaments à base d'**ABS**, d'**ASA** ou de **PS** dégagent des gaz nocifs durant l'impression. Il est donc interdit de les utiliser pendant les heures d'ouverture du fablab.

Dans la liste **Filament**, vous pouvez sélectionner un pré-réglage de filament dans la base de données du slicer. En cliquant sur l'option **Ajouter/Enlever des filaments**, vous avez accès au catalogue de filaments qui ont déja un pré-réglage.

![Liste "Filament"](img/filament.png){width=75%}

Si votre filament n'est pas dans la base de données, vous pouvez essayer un réglage générique. Par exemple, le réglage **Generic PLA** fonctionnera avec tous les filaments à base de PLA, mais pas toujours de façon optimale. L'idéal est de vérifier les réglages en les comparants avec les informations indiquées sur la bobine de filament que vous allez utiliser. Dans l'onglet **Réglages du filament**, vous pouvez modifier les températures de l'extrudeur et du plateau.

### Hauteur des couches

Par défaut, la hauteur de couche est à 0,2mm. C'est un bon compromis entre vitesse et qualité d'impression.

![Liste "Réglages d'impression"](img/layer.png){width=75%}

Pour un meilleur rendu des détails, sélectionnez une épaisseur de couche plus fine.

Pour une impression plus rapide, par exemple pour les très grosses pièces, sélectionnez 0,3mm.

Pour en savoir plus sur la hauteur de couches : [voir le site Prusa](https://help.prusa3d.com/fr/article/couches-et-perimetres_1748)

### Supports

Lorsqu'un élément est imprimé avec un angle de surplomb **inférieur à 45°**, il peut s'affaisser et nécessite un support pour le soutenir.

Les supports allongent le temps d'impression et sont parfois difficiles à retirer. Il faut donc évaluer s'ils sont vraiment nécéssaires.

Dans le slicer, vous avez 4 options pour les supports :

* Aucun (par défaut) : à utiliser uniquement pour des pièces sans surplomb et avec de ponts de petite taille
* Supports sur le plateau uniquement : permet d'éviter d'avoir des supports qui commencent dans la pièce (ils sont plus difficiles à retirer)
* Seulement pour les générateurs de supports : avec cette option, vous pouvez choisir où mettre des supports en ajoutant une forme à l'endroit voulu
* Partout : ajoute des supports partout où c'est nécéssaire

Dans l'onglet **Réglages d'impression**, sur la page **Supports** vous pouvez modifier les paramètres qui définissent où et comment les supports doivent être générés.

Pour un résultat plus net, il est parfois judicieux de jouer sur la distance de contact, l’espacement du motif d’interface, le nombre de couches d’interface, ...

Pour en savoir plus sur les supports : [voir le site Prusa](https://help.prusa3d.com/fr/article/supports_1698)

### Remplissage

Par défaut, la densité de remplissage est de 15%. C'est un bon compromis entre vitesse et solidité de l'impression.

Sélectionnez un remplissage entre 0% et 35%. Au-dessus de 35%, vous ne gagnez plus en solidité. Ce serait une perte de temps et un gaspillage de matière.

Attention, le remplissage sert aussi de support pour l'intérieur de la pièce. Si la couche supérieure a un profil horizontal, une densité de remplissage de minimum 10% est recommandée.

Dans l'onglet **Réglages d'impression**, sur la page **Remplissage** vous pouvez choisir le motif de remplissage.

Par défaut, il est sur **Gyroïde**. Ce motif s'imprime rapidement et il a l'avantage d'avoir la même solidité dans toutes les directions.

Voir le site Prusa pour en savoir plus sur [le remplissage](https://help.prusa3d.com/fr/article/remplissage_42) et sur [les différents motifs de remplissage](https://help.prusa3d.com/fr/article/motifs-de-remplissage_177130)

### Jupe et bordure

La jupe sert de test avant le début d'une impression, vous pouvez observer si elle s'imprime bien de façon identique sur tout son tracé. Si une partie de la jupe se décolle du plateau, c'est le signe d'une mauvaise adhérence au plateau. Il faut donc arrêter l'impression et corriger ce problème (voir le chapitre : Problèmes courants).

La bordure permet d'augmenter l'adhérence avec le plateau. C'est recommandé pour les pièces qui ont une petite surface en contact avec le plateau, en particulier si la hauteur de la pièce est grande.

Attention, il arrive que la bordure et la jupe se chevauchent, ce qui peut causer des problèmes à l'impression. Si vous activez la bordure, allez dans l'onglet **Réglages d'impression** sur la page **Jupe et bordure** et réglez la **Distance de l'objet** de la jupe pour qu'elle soit supérieure à la largeur de la bordure. Vous pouvez aussi désactiver la jupe en mettant le nombre de **Boucles** à zéro.

Pour en savoir plus sur la jupe et la bordure : [voir le site Prusa](https://help.prusa3d.com/fr/article/jupe-et-bordure_133969)

### Épaisseur des parois

Dans l'onglet **Réglages d'impression**, vous pouvez choisir le nombre de périmètres. Par défaut, le réglage est sur 2 périmètres, ce qui équivaut à une épaisseur de paroi d'environ 0,9mm.

Pour des parois plus solides, vous pouvez mettre 3 périmètres.

Il n'est pas recommandé d'en mettre 4 ou plus car ça n'augmenterait pas la solidité de la pièce et le temps d'impression serait plus long. Pour plus de solidité, mieux vaut augmenter la densité de remplissage.

Pour en savoir plus sur les périmètres : [voir le site Prusa](https://help.prusa3d.com/fr/article/couches-et-perimetres_1748#perimetres)

### Vérification et export du G-code

Cliquez sur le bouton **Découper maintenant** en bas à droite.

Vous pouvez alors voir une estimation du temps d'impression. Si le temps d'impression est trop long, vous pouvez encore modifier des réglages comme la hauteur de couche ou le remplissage par exemple.

Dans la fenêtre principale, vous pouvez voir une simulation de l'impression qui permet notamment de voir le niveau de détail attendu. Vous pouvez aussi voir l'intérieur de la pièce, couche par couche, en faisant glisser le curseur orange (à droite du plateau).

Si tout est ok, cliquez sur le bouton **Exporter le G-code**. Enregistrez le fichier G-code sur une carte SD **dans un dossier à votre nom**.

### Lancement de l'impression

* Vérifiez que le filament installé correspond bien à celui donné dans le slicer
* Nettoyez le plateau
* Insérez la carte SD dans le panneau de commande, une liste avec les fichiers présents sur la carte devrait s'afficher à l'écran

![](img/PrusaSlicer/PRUSA_40.jpeg){width=75%}
![](img/PrusaSlicer/PRUSA_41.jpeg){width=75%}
![](img/PrusaSlicer/PRUSA_42.jpeg){width=75%}
![](img/PrusaSlicer/PRUSA_43.jpeg){width=75%}

* Si la liste de fichiers ne s'affiche pas, retirez puis ré-insérez la carte SD.
* Utilisez le bouton rond pour sélectionner votre fichier G-code dans la liste
* Attendez la fin du pré-chauffage et de la calibration pour voir si la première couche adhère bien au plateau
* Si un message d'erreur demandant l'installation d'un nouveau firmware apparait, ignorez le en appuyant sur le bouton rond

![](img/PrusaSlicer/PRUSA_53.jpeg){width=75%}

### Exercices

[Liens vers les exercices](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/Exercises.md?ref_type=heads)

## Problèmes courants

### Mauvaise adhérence de l'impression au plateau

L'impression 3D nécéssite une bonne adhérence entre la première couche de plastique et le plateau.

Si l'adhésion est faible, la pièce risque de se décoller pendant l'impression.

Risques :

* Échec de l'impression
* Imprimante hors-service

Précautions :

* Ajouter une bordure (brim) dans le slicer pour les pièces ayant une faible surface à la base
* Vérifier s'il n'y a pas de résidus de plastique sous le plateau
* Nettoyer le plateau avant de lancer l'impression
* À chaque changement de filament, calibrer la hauteur de 1ère couche

Pour en savoir plus sur la calibration de la première couche : [voir le site Prusa](https://help.prusa3d.com/fr/article/calibration-de-la-premiere-couche-i3_112364)

### Nœuds dans la bobine de filament

Si le filament n'est pas fixé dans sa bobine, il y a un risque de formation de nœuds. Les nœuds ne sont pas toujours visibles et ils peuvent bloquer l'extrusion de filament pendant l'impression.

Risques :

* Sous-extrusion et impression ratée
* Encrassement et usure de l'extrudeur

Précautions :

* Quand on retire un filament, il faut le fixer directement dans sa bobine à l'aide d'un trou prévu à cet effet

![](img/PrusaSlicer/PRUSA_30.jpeg){width=75%}

* Au moment d'insérer un nouveau filament, le mettre directement dans l'extrudeur en ayant pris le soin de bien couper en biais le bout du filament.

![](img/PrusaSlicer/PRUSA_61.jpeg){width=75%}

Si vous constatez la présence d'un nœud durant l'impression, il est possible de mettre l'imprimante en pause le temps de défaire le nœud :

* Appuyez sur le bouton rond pour accéder au menu
* Sélectionnez l'option **Pause de l'impr.**
* Sélectionnez l'option **Décharger fil.**
* Retirez le filament de l'extrudeur et dénouez la bobine
* Coupez l'extrémité du filament avec une pince coupante
* Insérez le filament dans l'extrudeur
* Sélectionnez l'option **Charger filament**
* Attendez l'extrusion de plastique, puis confirmez en sélectionnant **Oui**
* Sélectionnez l'option **Reprendre impression.**

### Ancienne version du slicer/firmware

Il arrive qu'au moment de démarrer l'impression, l'écran de l'imprimante indique un message pour demander une mise-à-jour du firmware ou pour signaler que vous utilisez une ancienne version du slicer.

En général, ça ne posera pas de problème. Il suffit d'appuyer sur le bouton rond pour démarrer l'impression.

## Ressources

* [Design d'une pièce en vue de l'imprimer en 3D](https://help.prusa3d.com/fr/article/modelisation-en-pensant-a-limpression-3d_164135)
* [Matériaux de filaments d'impression 3D](https://help.prusa3d.com/fr/category/guide-des-materiaux_220)
