# Support de formation

## Imprimants Prusa i3 MK3S
[Tuto Impression 3D](3D-printers.md)

[Changement de taille de buse](Setting-Change.md)

[Exercices Formation 3D](Exercises.md)

## Imprimant Ultimaker

[Tuto](Ultimkaer.md)


Pour nous aider a améliorer nos tutorielle vous pouvez le faire directement via GitLab ou en nous envoyant un mail a fablab@ulb.be