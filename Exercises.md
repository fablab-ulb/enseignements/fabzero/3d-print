# Exercises

Lisez avant le 
[tutoriel ](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md?ref_type=heads)
pour l'impression 3D.

Si vous voulez plus d'information sur la création d'objet 3D vous pouvez visioner les viédos du chaine youtube de [Crea_Din 3D](https://www.youtube.com/@CreaDin3D)

## Liens d'objet rapide a imprimer

Une liste d'objets qui peuvent s'imprimer rapidement et facilement.

| Liste des objets |  Photo  |
| ------------ | ------------- |
| [pikachu](https://www.thingiverse.com/thing:376601)       | ![pikachu](img/exo/pikachu.png){width="50"}  |
| [Turtle](https://www.thingiverse.com/thing:2238443)       | ![turtle](img/exo/turtle.png){width="50"}  |
| [Eggs](https://www.thingiverse.com/thing:2829553)         | ![eggs](img/exo/eggs.png){width="50"}  |
| [Spinning Disk](https://www.thingiverse.com/thing:542914) | ![disk](img/exo/spinning-disk.png){width="50"}  |


## Printer Test

Des objets qui tests les capacités des imprimantes.

[Boat](https://www.thingiverse.com/thing:763622)

[Printer Test](https://www.thingiverse.com/thing:2656594)

[Cube](https://www.thingiverse.com/thing:1278865)
