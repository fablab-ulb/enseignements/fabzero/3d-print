# How to change the Default Printer Setting

## Updateing Prusa Slicer
In Prusa Slicer you can go to

1. Go to *Menu*
2. Select *Configuration*
3. Select *Check for Application Updates*

Follow the instruction of the updater.

![Update](img/Setting-Change/update.png){width=50%}

If at one point it asks you to create an account, you can just skip this step.
A prusa account is NOT needed. 
But you can make one if you wish to.

## Selecting a new printer / nozzle size

1. Select the gear next to the printer version
2. Select *Add/Remove preset*

![Update](img/Setting-Change/gear.png){width=50%}

If it request a *Configuration Update*, update it.
Generally you only need to click *OK*.

A new menu should appear automatically.

3. Go to *Prusa Reasearch* menu.
4. Deselect all printers and nozzle types.

![Update](img/Setting-Change/prusa.png){width=60%}

5. Scroll down till you find the *MK3 Familly*.
6. Select the **0.6 mm nozzle** within the **MK3S & MK3S+** category.

![Update](img/Setting-Change/mk3.png){width=75%}

7. Click next until you arrive into the *Filaments* menu. 
Or just select *Filaments* on the side menu.

![Update](img/Setting-Change/filament.png){width=75%}

8. Select the *None* button to deselect all the filament.

9. In the *Type* (2nd) collumn select PLA.
10. Than in *Profil* (4th) collumn select *Prusa PLA* **OR** *Generec PLA*.

11. Click *Finish*.

## Check

On the right side, make sure it shows as follow:

* Filament: PLA
* Printer: Original Prusa i3 MK3S & MK3S+ 0.6 nozzle

![Update](img/Setting-Change/check.png){width=50%}

If you need help you can come to our office.
If you want to help to improve this (or another) guide feel free to contact us at fablab@ulb.be .

# More links and help

Prusa Slicer [Documentation](https://help.prusa3d.com/article/configuration-wizard_1754)

A YT [video](https://www.youtube.com/watch?v=FD80xq3YdYE)
 showing how to access the configuration panel.
 If you follow this video make sure to select the correct printer !
 The printer needed is the __Origninal Prusa i3 MK3S & MK3S+ 0,6 nozzle__