# Procédure pour les imprimantes Ultimaker

1)	Rendez-vous sur le site d’Ultimaker et créer un compte. [https://ultimaker.com/fr/app/account-sign-up](https://ultimaker.com/fr/app/account-sign-up)
2)	Envoyer un mail à l’adresse guillaume.deneyer@ulb.be pour être ajouté au digital factory du Fablab.
3)	Télécharger Cura. [https://ultimaker.com/fr/software/ultimaker-cura](https://ultimaker.com/fr/software/ultimaker-cura)
4)	Une fois l’installation effectuée, lancer Cura et s’y connecter avec le compte Ultimaker créé plus tôt.
5)	Les imprimantes devraient directement être visibles et prêtes à imprimer !

![Ultimaker Cura](img/Cura.png)
